def binResearch(arr: list, item: int) -> int:
    l = 0
    r = len(arr) - 1  
    
    while l<=r:
        m = (l + r) // 2
        if arr[m] == item:
            return m
        elif arr[m] < item:
            l = m + 1
        else:
            r = m - 1
            
    return r + 1


if __name__ == "__main__":
    n = int(input())

    stickers = sorted(set(list(map(int, input().split()))))
        
    k = int(input())
    collectioners = list(map(int, input().split()))

    for i in collectioners:
        idx = binResearch(stickers, i)
        print(idx)
        

    