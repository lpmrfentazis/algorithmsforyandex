k = int(input())
string = input()

def calcBeautiful(string: str) -> int:
    maxx = 1
    tmp = 1
    for i, sym in enumerate(string):
        if i > 0:
            if sym == string[i-1]:
               tmp += 1 
            else:
               tmp = 1
               
            maxx = max(maxx, tmp)
            
    return maxx

print(calcBeautiful(string))