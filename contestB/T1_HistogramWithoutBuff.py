from collections import Counter



with open("input.txt", "r") as f:
    string = "".join(f.readlines())

string = string.replace("\n", "")
string = string.replace(" ", "")

cnt = Counter(string)
maxx = cnt.most_common(1)[0][1]
symbols = sorted(cnt.keys())

counts = sorted(cnt.most_common(), key=lambda x: ord(x[0]))

for i in range(maxx, 0, -1):
    out = ""
    for sym in counts:
        if i <= sym[1]:
            out += "#"
            
        else:
            out += " "    
    print(out)

print("".join(symbols))