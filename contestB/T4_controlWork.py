count = int(input())
variants = int(input())
row = int(input())
place = int(input()) - 1

pos = 2 * row if place else 2 * row - 1
var = (pos + 1) // variants

targetRange = -1
targetPos = -1
direction = 1

if pos + variants < count:
    targetPos = pos + variants
    targetRange = (targetPos+1) // 2 - row
  
if pos - variants > 0:
    tp = pos - variants
    t = row - (tp+1) // 2
    if targetRange != -1 and t < targetRange:
        targetRange = t 
        targetPos = tp
        direction = -1

if targetRange == -1:
    print(-1)
else:
    print(row + targetRange * direction, (targetPos+1) % 2 + 1)
