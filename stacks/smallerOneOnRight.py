"""
    Для каждого числа в последовательности определить ближайший меньший элемент справа
    O(n)    
"""

if __name__ == "__main__":
    numbers = list(map(int, input().split()))
    answer = [ [i, -1] for i in numbers]
    stack = []

    for i, elem in enumerate(numbers):
        if len(stack) == 0:
            stack.append((elem, i))
        else:
            if elem >= stack[-1][0]:
                stack.append((elem, i))
                continue
                
            while elem < stack[-1][0]:
                _, j = stack.pop()
                answer[j][1] = i  
                if not len(stack):
                    break
            stack.append((elem, i))
                
    for i, elem in enumerate(answer):
        print(i, *elem)  